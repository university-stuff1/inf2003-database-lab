-- Question 1;
SELECT email FROM student;

-- Question 2;
SELECT DISTINCT email FROM student;

-- Question 3
SELECT name FROM student ORDER BY name DESC;

-- Question 4
SELECT temp1.* FROM student temp1
INNER JOIN (
    SELECT name
    FROM student
    GROUP BY name
    HAVING COUNT(*) > 1
) temp2 ON temp1.name = temp2.name;

-- Question 5
SELECT DISTINCT name FROM student ORDER BY name ASC;

-- Question 6
SELECT name FROM student 
INNER JOIN copy ON student.email = copy.owner
WHERE copy.book = '978-0321474049';

-- Question 7
SELECT student.name FROM student
INNER JOIN copy ON student.email = copy.owner
INNER JOIN book ON copy.book = book.ISBN13
WHERE book.pages > 100 AND LOWER(book.title) LIKE '%photoshop%';

-- Question 8
SELECT CEIL(SUM(pages) / 2.0) FROM book
WHERE ISBN13 IN ('978-0321474049', '978-0684801520');

-- Question 9
SELECT DISTINCT student.name FROM student
INNER JOIN copy ON student.email = copy.owner
INNER JOIN book ON copy.book = book.ISBN13
WHERE book.ISBN13 <> '978-0321474049';

-- Question 10
SELECT DISTINCT student.name FROM student
INNER JOIN loan ON student.email = loan.borrower
INNER JOIN copy ON loan.owner = copy.owner AND loan.book = copy.book
INNER JOIN book ON copy.book = book.ISBN13
WHERE book.ISBN13 = '978-0321474049';

-- Question 11
SELECT student.name FROM student
INNER JOIN copy ON student.email = copy.owner
INNER JOIN book ON copy.book = book.ISBN13
WHERE book.ISBN13 = '978-0321474049'
UNION
SELECT student.name FROM student
INNER JOIN loan ON student.email = loan.borrower
INNER JOIN copy ON loan.owner = copy.owner AND loan.book = copy.book
INNER JOIN book ON copy.book = book.ISBN13
WHERE book.ISBN13 = '978-0321474049';
