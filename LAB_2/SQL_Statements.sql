---------Task 1
-- NOTE: Login as root..

-- Create user locally, with password set as `lmaoman`.
-- `@localhost` means that the user can only be connected locally.
CREATE USER 'yeo_wen_qin'@'localhost' IDENTIFIED BY 'lmaoman';

-- Grant privileges to all the existing database to the user.
-- (Also, allow user permission to create new database, etc)
GRANT ALL PRIVILEGES ON *.* TO 'yeo_wen_qin'@'localhost' WITH GRANT OPTION;
-- To apply the change
FLUSH PRIVILEGES;

-- Show
SELECT host, user FROM mysql.user;



---------Task 2
-- NOTE: Logout as root, then login as the new local user...

CREATE DATABASE yeo_wen_qin_db;
-- Switch to the database context...
USE yeo_wen_qin_db;

-- Create `students` table.
CREATE TABLE students (
    sid INT PRIMARY KEY,
    name VARCHAR(255),
    grade VARCHAR(2)
);

SHOW databases;



-----------Task 3
-- NOTE: In the same database context...
SELECT * FROM students;



-----------Task 4
-- NOTE: In the same database context...
INSERT INTO students (sid, name, grade) VALUES (2001111, 'Lewis', 'NA');
INSERT INTO students (sid, name) VALUES (2002222, 'Valtteri');
INSERT INTO students (sid, name) VALUES (2003333, 'Michael');

SELECT * FROM students;



-----------Task 5
-- NOTE: In the same database context...

-- Possible to update by `name` as well,
-- but there might be rare cases where students have same name.
UPDATE students SET grade = 'A+' WHERE sid = 2003333;
SELECT * FROM students;

UPDATE students SET grade = 'A-' WHERE sid = 2002222;
SELECT * FROM students;



-----------Task 6
-- NOTE: In the same database context...

DELETE FROM students WHERE sid = 2002222;
SELECT * FROM students;

-- Delete all rows in `students` table.
DELETE FROM students;
SELECT * FROM students;

DROP DATABASE yeo_wen_qin_db;
SHOW databases;